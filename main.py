#Importing modules
from modules.login import *
from modules.billing import billing
from modules.calculator import calculator
from modules.stocks import *
from modules.settings import *
import colorama
import os
colorama.init()
print(colorama.Fore.CYAN)
user = login()
#os.system("clear")
if user =="ADMIN":
    while True:
        print(colorama.Fore.YELLOW)
        c = input("1)Stocks\n2)Billing\n3)Calculator\n4)Basic settings\n>")
        if c=="1":
            stocks()
            break
        elif c=="2":
            billing()
            break
        elif c=="3":
            calculator()
            break
        elif c=="4":
            setting()
        else:
            print(colorama.Fore.RED+"OPTION NOT AVAILABLE ")
else:
    print("LOGGED IN AS NORMAL USER ")
    while True:
        
        c = input("1)Billing \n2)Calculator \n>")
        if c =="1":
            #os.system('clear')
            billing()
            break
        if c=="2":
          #  os.system("clear")
            calculator()
            break
        else:
            print(colorama.Fore.RED+"OPTION NOT AVAILABLE ")
    

print(colorama.Fore.GREEN+"BYE\nTHNAK YOU FOR USING OUR SOFTWARE")
