# This function adds two numbers
def add(x, y):
    return x + y
# This function subtracts two numbers
def subtract(x, y):
    return x - y
# This function multiplies two numbers
def multiply(x, y):
    return x * y
# This function divides two numbers
def divide(x, y):
    return x / y
def calculator():
    print("Select operation.\n1.Add\n2.Subtract\n3.Multiply\n4.Divide")
    while True:
       print("Select operation.\n1.Add\n2.Subtract\n3.Multiply\n4.Divide\n5. Exit")
   # take input from the user
       choice = input("Enter choice(1/2/3/4): ")
       # check if choice is one of the four options
       if choice in ('1', '2', '3', '4'):
            num1 = float(input("Enter first number: "))
            num2 = float(input("Enter second number: "))
       if choice == '1':
             print(num1, "+", num2, "=", add(num1, num2))
       elif choice == '2':
            print(num1, "-", num2, "=", subtract(num1, num2))
       elif choice == '3':
            print(num1, "*", num2, "=", multiply(num1, num2))
       elif choice == '4':
            print(num1, "/", num2, "=", divide(num1, num2))
       elif choice == '5':
            break    
            
            # check if user wants another calculation
            # break the while loop if answer is no
            next_calculation = input("Let's do next calculation? (yes/no): ")
            if next_calculation == "no":
              break
       else:
            print("Invalid Input")
