#Importing modules
from  modules.mysql_project import *
import colorama 
from colorama import Fore,Style,Back
from beautifultable import BeautifulTable
import os
colorama.init()
#Defining the function 
def billing():    
    bill={}
    while True:
        print(Fore.YELLOW)
        x=input('1. Append an item\n2. Removing an item\n3. Displaying information of a particular item\n4. Displaying the items in cart\n5. CHECKOUT\n6. Exit\n> ')
        #os.system("cls")
        #Below will append an item into a bill
        if x=="1":
            code=input('Enter the code of the item you want to add to the bill: ')
            append(code,bill)
        #Below will remove an item from a bill
        elif x=="2":
            code=input('Enter the code of the item you want to remove from the bill: ')   
            remove(code,bill)
        #Below will display the information of a particular item    
        elif x=="3":
            code=input('Enter the code of that item whose information is to be displayed: ')
            print(Fore.CYAN+str(displayitem(code)))
        #It shows the items in your cart
        elif x=="4":
            table = BeautifulTable()
            total=0
            table.columns.header = ["ITEM_CODE", "ITEM_NAME","QUANTITY","RATE PER ITEM",'CATEGORY',"TOTAL COST/ITEM"]
            for key,value in bill.items():
                value.append(value[2]*value[3])
                table.rows.append(value)
                total +=value[5]
            table.rows.append([" "," "," "," ","TOTAL",total])
            print(Fore.CYAN)
            print(table)
            print(Fore.CYAN)
        #Below will checkout
        elif x=="5":
            if bill!={}:
                import pickle
                infofile =open("Backend/information.dat","rb")
                try:
                    r=pickle.load(infofile)
                except:
                    pass
                infofile.close()
                print(Back.BLACK)
                print(Fore.WHITE)
                print('STORE_ID: ',r[0])
                print('GST_ID: ',r[3]+"\n")
                print(table)
                print('ADDRESS: ',r[2])
                print('MOBILE_NUMBER',r[1])
                print('DISCLAIMER',r[4]+"\n")
                print("THANK YOU FOR VISITING OUR STORE \nVISIT AGAIN  :)")
                print(Back.RESET)
                break
            else:
                print(Fore.RED+"BILL IS EMPTY")
        elif x=="6":
            exit()
        else:
            print(Fore.RED+"Entered option is not in menu")        
    


            
        
