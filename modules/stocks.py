#Importing modules
import colorama
from modules.mysql_init import *
from beautifultable import BeautifulTable
colorama.init()
#Defing the function 
def stocks():
    while True:
        print(colorama.Fore.GREEN+'1. VIEW STOCKS\n2. ADDING NEW ITEM IN  STOCKS\n3. REMOVING STOCKS\n4. APPENDING STOCKS \n5. Exit\n')
        x=input('> ')
        #Below code will show full stock
        if x=="1":
            mysql_csr.execute('SELECT * from productInfo;')
            data=mysql_csr.fetchall()
            table = BeautifulTable()
            table.columns.header=["ITEM CODE",'ITEM NAME','STOCK','PRICE','CATEGORY']
            for row in data:
                table.rows.append(row)            
            print(colorama.Fore.CYAN)
            print(table)
            print(colorama.Fore.RESET)
        #Below code will show ADD new item to stock
        elif x=="2":
            item_code=input("Enter the Item code : ")
            item_NAME=input("Enter the Item name : ")
            item_stocks=input('Enter the number of items: ')
            item_price =input("Enter the rate per item: ")
            category_item=input('Enter the category of that item: ')
            mysql_csr.execute(f"insert into productInfo values('{item_code}','{item_NAME}',{item_stocks},{item_price},'{category_item}');")
            ms.commit()
            print(colorama.Fore.RED+"ITEM ADDED TO DATABASE") 
        #Below code will REMOVE an item from stock
        elif x=="3":
            code=input("Enter the code of the item you want to remove from database: ")
            try:
                mysql_csr.execute(f"DELETE FROM productInfo WHERE Item_code='{code}';  ")
                print(colorama.Fore.RED+"Item removed from database ")
                ms.commit()
            except:
                print(colorama.Fore.RED+"SOME ERROR HAPPENED AT OUR END :-(")
        #Below code will increase the number of items in stock
        elif x=="4":
            code = input("Enter the code of the item of which you want to increase: ")
            new_quantity = int(input("Enter the number of new item : "))
            try:
                mysql_csr.execute(f"UPDATE productInfo set stocks =stocks+{new_quantity} where Item_code='{code.strip()}'")
                ms.commit()
            except :
                print(colorama.Fore.RED+f"SOME ERROR HAPPENED AT OUR END :-(\n ERROR ")                    
        #EXIT
        elif x=="5":
            break
        else:
            print(colorama.Fore.RED+"INVALID OPTION !!!")
